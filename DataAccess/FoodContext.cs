﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace DataAccess
{
    class FoodContext : DbContext
    {
        public FoodContext():base("Server =.; Database=Food;Trusted_Connection=True;")
        {
            
        }

        public DbSet<User> Users { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get {
                return FirstName + LastName;
                                } }
        public int Id { get; set; }
        public DateTime Birthday { get; set; }


    }
}
